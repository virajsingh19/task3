import React, {Component} from 'react';
import './Header.css';
class Header extends Component {
    render() {
        return (
            <div className="Header pl4 pa1 white f3">
                Home
            </div>
        );
    }
}

export default Header;