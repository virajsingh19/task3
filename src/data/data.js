module.exports = [{
    id: 1,
    title: "Product1",
    length:'1:00',
    category:'Python',
    author:'Author 1'
}, {
    id: 2,
    title: "Product2",
    length:'0:20',
    category:'Python',
    author:'Author 2'
},
{
    id: 3,
    title: "Product3",
    length:'1:40',
    category:'Python',
    author:'Author 3'
},
{
    id: 4,
    title: "Product4",
    length:'5:10',
    category:'Python',
    author:'Author 4'
}
];